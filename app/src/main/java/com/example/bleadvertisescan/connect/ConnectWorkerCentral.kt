package com.example.bleadvertisescan.connect

import android.bluetooth.*
import android.bluetooth.BluetoothAdapter.*
import android.content.Context
import android.util.Log
import com.example.bleadvertisescan.advertise.AdvertiseWorker
import com.example.bleadvertisescan.advertise.AdvertiseWorker.Companion.psmCharacteristic
import com.example.bleadvertisescan.model.BleItemCentral
import com.example.bleadvertisescan.model.Connection
import com.example.bleadvertisescan.model.Device
import com.example.bleadvertisescan.model.LogName
import com.example.bleadvertisescan.repository.BleRepository
import java.nio.ByteBuffer
import java.util.*
import kotlin.collections.HashMap

class ConnectWorkerCentral(
    private val bluetoothAdapter: BluetoothAdapter,
    private val context: Context
) : BluetoothGattCallback() {
    private var bleItems = HashMap<String, BleItemCentral>()

    fun connectByMacAddress(model: Device) {
        bluetoothAdapter.getRemoteDevice(model.address)?.let { device ->
            bleItems[model.name] = BleItemCentral()
            bleItems[model.name]?.gatt = device.connectGatt(context, false, this)
            Log.d(
                LogName.BleConnect.name,
                "try to connect ${bleItems[model.name]?.gatt?.device?.name}"
            )
        }
    }

    fun disconnectAll() {
        for (item in bleItems.values) {
            item.gatt?.discoverServices()
            item.gatt?.disconnect()
            item.gatt?.close()
        }
        bleItems.clear()
    }

    fun disconnect(name: String) {
        BleRepository.connectObserver.resultOfConnection(
            result = Connection(
                device = Device(
                    name = bleItems[name]!!.gatt?.device!!.name,
                    address = bleItems[name]!!.gatt?.device!!.address
                ), state = "", cancel = true
            )
        )
        bleItems[name]?.gatt?.disconnect()
    }

    private fun bytesToInt(bytes: ByteArray): Int {
        val buffer = ByteBuffer.allocate(Int.SIZE_BYTES)
        buffer.put(bytes, 0, bytes.size)
        buffer.flip()
        return buffer.int
    }

    override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
        Log.d(
            LogName.BleConnect.name,
            "onConnectionStateChange newState: $status -> $newState Central name: ${gatt?.device?.name} address: ${gatt?.device?.address}"
        )

        bleItems[gatt?.device?.name]?.let { bleItem ->
            when {
                STATE_CONNECTED == newState -> {
                    bleItem.gatt?.discoverServices()
                }
                STATE_DISCONNECTED == newState -> {
                    bleItem.gatt?.close()
                    bleItem.socket?.close()
                    bleItem.socket = null
                }
                else -> {  }
            }
            BleRepository.connectObserver.resultOfConnection(
                result = Connection(
                    device = Device(
                        name = bleItem.gatt?.device?.name!!,
                        address = bleItem.gatt?.device?.address!!
                    ),
                    state = when (newState) {
                        STATE_DISCONNECTED -> "disconnected"
                        STATE_CONNECTING -> "connecting"
                        STATE_CONNECTED -> "connected"
                        STATE_DISCONNECTING -> "disconnecting"
                        else -> "Unknown $newState"
                    }, cancel = false
                )
            )
        } ?: BleRepository.connectObserver.resultOfConnection(
            result = Connection(
                device = Device(
                    name = "null",
                    address = "null"
                ), state = "Name error", cancel = false
            )
        )

    }

    override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
        super.onServicesDiscovered(gatt, status)
        Log.d(LogName.BleConnect.name, "onServicesDiscovered")

        bleItems[gatt?.device?.name]?.let { bleItem ->
            val service = bleItem.gatt!!.getService(UUID.fromString(AdvertiseWorker.APP_UUID))
            for (item in service.characteristics) {
                if (item.uuid == UUID.fromString(psmCharacteristic)) {
                    bleItem.characteristicRead = item
                }
            }

            bleItem.gatt!!.setCharacteristicNotification(bleItem.characteristicRead, true)
            bleItem.characteristicRead?.descriptors?.get(0)?.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            bleItem.gatt!!.writeDescriptor(bleItem.characteristicRead?.descriptors?.get(0))
        }
    }

    override fun onCharacteristicChanged(
        gatt: BluetoothGatt?,
        characteristic: BluetoothGattCharacteristic?
    ) {
        super.onCharacteristicChanged(gatt, characteristic)
        Log.d(LogName.BleConnect.name, "onCharacteristicChanged ${gatt?.device?.name}")
        bleItems[gatt?.device?.name]?.let { bleItem ->
            bleItem.psm = bytesToInt(characteristic?.value!!)
            bleItem.socket = bleItem.gatt?.device?.createInsecureL2capChannel(bleItem.psm!!)
            getDefaultAdapter().cancelDiscovery()
            bleItem.socket!!.connect()
            Log.d(LogName.BleConnect.name, "psm: $${bleItem.psm}")
        }
    }
}