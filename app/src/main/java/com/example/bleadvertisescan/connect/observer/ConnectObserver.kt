package com.example.bleadvertisescan.connect.observer

import com.example.bleadvertisescan.model.Connection
import com.example.bleadvertisescan.scan.observer.W2Observer

class ConnectObserver(
    map: HashMap<String, IConnectObserver>
) : W2Observer<IConnectObserver>(map = map), IConnectObserver {
    override fun resultOfConnection(result: Connection) {
        secureMap.forEach { sub -> sub.resultOfConnection(result = result) }
    }
}