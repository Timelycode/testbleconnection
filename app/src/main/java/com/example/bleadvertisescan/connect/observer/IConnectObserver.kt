package com.example.bleadvertisescan.connect.observer

import com.example.bleadvertisescan.model.Connection

interface IConnectObserver {
    fun resultOfConnection(result: Connection)
}