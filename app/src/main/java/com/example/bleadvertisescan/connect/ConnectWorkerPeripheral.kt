package com.example.bleadvertisescan.connect

import android.annotation.SuppressLint
import android.bluetooth.*
import android.bluetooth.BluetoothAdapter.STATE_CONNECTED
import android.util.Log
import com.example.bleadvertisescan.advertise.AdvertiseWorker
import com.example.bleadvertisescan.advertise.AdvertiseWorker.Companion.descriptorUUID
import com.example.bleadvertisescan.advertise.AdvertiseWorker.Companion.psmCharacteristic
import com.example.bleadvertisescan.model.Connection
import com.example.bleadvertisescan.model.Device
import com.example.bleadvertisescan.model.LogName
import com.example.bleadvertisescan.repository.BleRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.nio.ByteBuffer
import java.util.*

class ConnectWorkerPeripheral(private val adapter: BluetoothAdapter) :
    BluetoothGattServerCallback() {
    private var serverSocket: BluetoothServerSocket? = null
    lateinit var mBluetoothGattServer: BluetoothGattServer
    var deviceBle: BluetoothDevice? = null
    lateinit var characteristic: BluetoothGattCharacteristic

    override fun onDescriptorWriteRequest(
        device: BluetoothDevice?,
        requestId: Int,
        descriptor: BluetoothGattDescriptor?,
        preparedWrite: Boolean,
        responseNeeded: Boolean,
        offset: Int,
        value: ByteArray?
    ) {
        super.onDescriptorWriteRequest(
            device,
            requestId,
            descriptor,
            preparedWrite,
            responseNeeded,
            offset,
            value
        )
        Log.d(LogName.BleConnect.name, "onDescriptorWriteRequest")
        sendPsm(device = device!!)
    }

    fun initService() {
        val service = BluetoothGattService(
            UUID.fromString(AdvertiseWorker.APP_UUID),
            BluetoothGattService.SERVICE_TYPE_PRIMARY
        )

        characteristic = BluetoothGattCharacteristic(
            UUID.fromString(psmCharacteristic),
            BluetoothGattCharacteristic.PROPERTY_READ or BluetoothGattCharacteristic.PROPERTY_NOTIFY,
            BluetoothGattCharacteristic.PERMISSION_READ
        )

        val descriptor = BluetoothGattDescriptor(
            UUID.fromString(descriptorUUID),
            BluetoothGattDescriptor.PERMISSION_WRITE or
                    BluetoothGattDescriptor.PERMISSION_READ
        )
        descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
        descriptor.value = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
        characteristic.addDescriptor(descriptor)

        service.addCharacteristic(characteristic)

        mBluetoothGattServer.addService(service)
    }

    private fun intToByte(elem: Int): ByteArray {
        val buffer = ByteBuffer.allocate(Int.SIZE_BYTES)
        buffer.putInt(elem)
        val array = buffer.array()
        buffer.clear()
        return array
    }


    private fun sendPsm(device: BluetoothDevice) {
        GlobalScope.launch(Dispatchers.IO) {
            serverSocket = adapter.listenUsingInsecureL2capChannel()
            withContext(Dispatchers.IO) {
                Log.d(LogName.BleConnect.name, "sendPsm: ${serverSocket!!.psm}")
                characteristic.value = intToByte(serverSocket!!.psm)
                mBluetoothGattServer.notifyCharacteristicChanged(device, characteristic, false)
            }
            try {
                serverSocket?.accept()
                BleRepository.connectObserver.resultOfConnection(
                    result = Connection(
                        device = Device(
                            name = device.name ?: "Unknown",
                            address = device.address
                        ), state = "connected L2cap", cancel = false
                    )
                )
            } catch (e: Exception) {
                BleRepository.connectObserver.resultOfConnection(
                    result = Connection(
                        device = Device(
                            name = device.name ?: "Unknown",
                            address = device.address
                        ), state = "error L2cap", cancel = false
                    )
                )
            }
        }
    }

    @SuppressLint("HardwareIds")
    override fun onConnectionStateChange(device: BluetoothDevice?, status: Int, newState: Int) {
        Log.d(
            LogName.BleConnect.name,
            "onConnectionStateChange newState: $status -> $newState Peripheral address: ${device?.address}"
        )
        if (STATE_CONNECTED == newState) {
            deviceBle = device
            BleRepository.connectObserver.resultOfConnection(
                result = Connection(
                    device = Device(
                        name = device?.name ?: "Unknown",
                        address = device?.address ?: "00:00:00:00:00:00"
                    ), state = "connection by BLE", cancel = false
                )
            )
        } else if (BluetoothAdapter.STATE_DISCONNECTED == newState) {
            serverSocket?.close()
            serverSocket = null
            BleRepository.scanObserver.lostDevices()
        }
    }
}