package com.example.bleadvertisescan.scan.observer

abstract class W2Observer<T>(private val map: HashMap<String, T> = hashMapOf()) {
    protected val secureMap: List<T>
        get() {
            val list = arrayListOf<T>()
            map.forEach { list.add(it.value) }
            return list
        }

    fun subscribe(name: String, sub: T) {
        map[name] = sub
    }

    fun clearAllSubscribers() {
        map.clear()
    }

    fun unsubscribe(name: String) {
        map.remove(name)
    }
}