package com.example.bleadvertisescan.scan.observer

import com.example.bleadvertisescan.model.Device

class ScanObserver(
    map: HashMap<String, IScanObserver>
) : W2Observer<IScanObserver>(map = map), IScanObserver {
    override fun foundDevice(mac: Device) {
        secureMap.forEach { sub -> sub.foundDevice(mac = mac) }
    }

    override fun lostDevices() {
        secureMap.forEach { sub -> sub.lostDevices() }
    }
}