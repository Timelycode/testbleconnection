package com.example.bleadvertisescan.scan

import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.os.ParcelUuid
import android.util.Log
import com.example.bleadvertisescan.advertise.AdvertiseWorker.Companion.APP_UUID
import com.example.bleadvertisescan.model.Device
import com.example.bleadvertisescan.model.LogName
import com.example.bleadvertisescan.repository.BleRepository

class ScanWorker(private val bluetoothAdapter: BluetoothAdapter) : ScanCallback() {
    fun startScan() {
        val settings = ScanSettings
            .Builder()
            .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
            .build()

        val filters = arrayListOf(
            ScanFilter.Builder()
                .setServiceUuid(ParcelUuid.fromString(APP_UUID))
                .build()
        )

        bluetoothAdapter.bluetoothLeScanner?.startScan(filters, settings, this)
        Log.d(LogName.Scan.name, "startScan")
    }

    fun stopScan() {
        bluetoothAdapter.bluetoothLeScanner?.stopScan(this)
        Log.d(LogName.Scan.name, "stopScan")
    }

    override fun onScanResult(callbackType: Int, result: ScanResult?) {
        result?.device?.let { phone ->
            BleRepository.scanObserver.foundDevice(
                mac = Device(
                    name = phone.name ?: "No name",
                    address = phone.address
                )
            )
            Log.d(LogName.Scan.name, "${phone.name} ${phone.address}")
        }
    }

    override fun onScanFailed(errorCode: Int) {
        Log.d(LogName.Scan.name, "onScanFailed errorCode: $errorCode")
    }
}