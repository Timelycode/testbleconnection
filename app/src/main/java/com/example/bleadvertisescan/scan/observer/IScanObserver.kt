package com.example.bleadvertisescan.scan.observer

import com.example.bleadvertisescan.model.Device

interface IScanObserver {
    fun foundDevice(mac: Device)
    fun lostDevices()
}