package com.example.bleadvertisescan.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bleadvertisescan.databinding.ItemDeviceBinding
import com.example.bleadvertisescan.model.BleRole
import com.example.bleadvertisescan.model.DeviceState
import com.example.bleadvertisescan.repository.BleRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DevicesAdapter(private val list: ArrayList<DeviceState>) : RecyclerView.Adapter<DevicesViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DevicesViewHolder {
        return DevicesViewHolder(
            bin = ItemDeviceBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: DevicesViewHolder, position: Int) {
        if (BleRepository.role == BleRole.Central) {
            holder.setParamsCentral(model = list[position])
        } else {
            holder.setParamsPeripheral(model = list[position])
        }
    }

    override fun getItemCount() = list.size

    @SuppressLint("NotifyDataSetChanged")
    fun addDevice(bleDevice: DeviceState) {
        list.add(bleDevice)
        GlobalScope.launch(Dispatchers.Main) { notifyDataSetChanged() }
    }

    fun updateDevice(bleDevice: DeviceState) {
        val index = list.indexOfFirst { bleDevice.device.name == it.device.name }
        if (index != -1) {
            list[index].device = bleDevice.device
            list[index].connectionState = bleDevice.connectionState
            list[index].cancelState = bleDevice.cancelState
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clear() {
        list.clear()
        GlobalScope.launch(Dispatchers.Main) { notifyDataSetChanged() }
    }
}