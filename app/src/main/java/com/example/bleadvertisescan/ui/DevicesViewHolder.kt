package com.example.bleadvertisescan.ui

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.bleadvertisescan.databinding.ItemDeviceBinding
import com.example.bleadvertisescan.model.Connection
import com.example.bleadvertisescan.model.Device
import com.example.bleadvertisescan.model.DeviceState
import com.example.bleadvertisescan.repository.BleRepository

class DevicesViewHolder(private val bin: ItemDeviceBinding) : RecyclerView.ViewHolder(bin.root) {
    @SuppressLint("SetTextI18n")
    fun setParamsCentral(model: DeviceState) {
        bin.apply {
            textDevice.text = "Name: ${model.device.name}\n${model.device.address}\n${model.connectionState}"
            textDevice.setOnClickListener {
                BleRepository.connectObserver.resultOfConnection(
                    result = Connection(
                        device = Device(
                            name = model.device.name,
                            address = model.device.address
                        ), state = "trying", cancel = false
                    )
                )
                BleRepository.connectToDevice(
                    device = Device(
                        name = model.device.name,
                        address = model.device.address
                    )
                )
            }
            textDisconnect.setOnClickListener {
                BleRepository.disconnectFromDevice(name = model.device.name)
            }
            textDisconnect.text = "Disconnect\n${
                if (model.cancelState) {
                    "cancel"
                } else {
                    ""
                }
            }"
        }
    }

    fun setParamsPeripheral(model: DeviceState) {
        setParamsCentral(model = model)
        bin.textDevice.setBackgroundColor(Color.BLUE)
        bin.textDisconnect.isEnabled = false
        bin.textDisconnect.visibility = View.GONE
    }
}