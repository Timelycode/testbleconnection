package com.example.bleadvertisescan.ui

import android.annotation.SuppressLint
import android.bluetooth.BluetoothManager
import android.content.Context
import com.example.bleadvertisescan.advertise.observer.IAdvertiseObserver
import com.example.bleadvertisescan.connect.observer.IConnectObserver
import com.example.bleadvertisescan.model.BleRole
import com.example.bleadvertisescan.model.Connection
import com.example.bleadvertisescan.model.Device
import com.example.bleadvertisescan.model.DeviceState
import com.example.bleadvertisescan.repository.BleRepository
import com.example.bleadvertisescan.scan.observer.IScanObserver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class MainActivityPresenter(
    private val stateView: IMainActivityView,
    private val bluetoothManager: BluetoothManager,
    val adapter: DevicesAdapter
) : IScanObserver, IConnectObserver, IAdvertiseObserver {
    private val timer = Timer()
    private var task: TimerTask? = null
    private val devices = arrayListOf<DeviceState>()

    fun subscribe() {
        BleRepository.scanObserver.subscribe(
            name = MainActivityPresenter::class.java.name,
            sub = this
        )
        BleRepository.connectObserver.subscribe(
            name = MainActivityPresenter::class.java.name,
            sub = this
        )
        BleRepository.advertiseObserver.subscribe(
            name = MainActivityPresenter::class.java.name,
            sub = this
        )
        task = object : TimerTask() {
            @SuppressLint("NotifyDataSetChanged")
            override fun run() {
                GlobalScope.launch(Dispatchers.Main) { adapter.notifyDataSetChanged() }
            }
        }
        timer.schedule(task, 5000, 5000)
    }

    fun unsubscribe() {
        BleRepository.scanObserver.unsubscribe(name = MainActivityPresenter::class.java.name)
        BleRepository.connectObserver.unsubscribe(name = MainActivityPresenter::class.java.name)
        task?.cancel()
        task = null
    }

    fun selectRole(newRole: BleRole, context: Context) {
        GlobalScope.launch(Dispatchers.IO) {
            BleRepository.selectRole(
                newRole = newRole,
                bManager = bluetoothManager,
                context = context
            )
        }
    }

    override fun foundDevice(mac: Device) {
        devices.firstOrNull { it.device.name == mac.name }?.let { local ->
            local.device.address = mac.address
            adapter.updateDevice(bleDevice = local)
        } ?: run {
            devices.add(
                DeviceState(
                    device = mac,
                    connectionState = "No connect",
                    cancelState = false
                )
            )
            adapter.addDevice(bleDevice = devices.last())
        }

    }

    override fun lostDevices() {
        adapter.clear()
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun resultOfConnection(result: Connection) {
        GlobalScope.launch(Dispatchers.Main) {
            devices.firstOrNull { it.device.name == result.device.name }?.let { local ->
                if (result.state != "") {
                    local.connectionState = result.state
                }
                local.cancelState = result.cancel
                adapter.updateDevice(bleDevice = local)
                GlobalScope.launch(Dispatchers.Main) { adapter.notifyDataSetChanged() }
            } ?: adapter.addDevice(
                bleDevice = DeviceState(
                    device = result.device,
                    connectionState = result.state,
                    cancelState = result.cancel
                )
            )
        }
    }

    override fun changeStateOfAdvertising(advertising: Boolean) {
        GlobalScope.launch(Dispatchers.Main) {
            stateView.notifyAboutEvent(
                event = if (advertising) {
                    "Advertising: working"
                } else {
                    "Advertising: relaxing"
                }
            )
        }
    }
}