package com.example.bleadvertisescan.ui

interface IMainActivityView {
    fun notifyAboutEvent(event: String)
}