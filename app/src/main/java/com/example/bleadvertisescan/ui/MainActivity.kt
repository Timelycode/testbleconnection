package com.example.bleadvertisescan.ui

import android.Manifest
import android.bluetooth.BluetoothManager
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bleadvertisescan.R
import com.example.bleadvertisescan.databinding.ActivityMainBinding
import com.example.bleadvertisescan.model.BleRole
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity(), IMainActivityView {
    companion object {
        private const val requestPermission = 3145
    }

    private lateinit var binding: ActivityMainBinding
    private lateinit var presenter: MainActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        askPermission()
        GlobalScope.launch(Dispatchers.IO) {
            presenter = MainActivityPresenter(
                stateView = this@MainActivity,
                adapter = DevicesAdapter(list = arrayListOf()),
                bluetoothManager = this@MainActivity.getSystemService(
                    BLUETOOTH_SERVICE
                ) as BluetoothManager
            )
            presenter.subscribe()
            withContext(Dispatchers.Main) {
                binding.groupConnection.setOnCheckedChangeListener { group, checkedId ->
                    presenter.selectRole(
                        context = this@MainActivity, newRole = when (checkedId) {
                            R.id.radio_central -> BleRole.Central
                            R.id.radio_peripheral -> BleRole.Peripheral
                            else -> BleRole.None
                        }
                    )
                }

                binding.recyclerDevices.adapter = presenter.adapter
                binding.recyclerDevices.layoutManager = LinearLayoutManager(this@MainActivity)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribe()
    }

    override fun notifyAboutEvent(event: String) {
        binding.textResult.text = event
    }

    fun askPermission() {
        val permissions = getPermissions()
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(this, permissions, Companion.requestPermission)
                break
            }
        }
    }

    private fun getPermissions() = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION
    )
}