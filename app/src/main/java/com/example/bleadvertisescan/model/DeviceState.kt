package com.example.bleadvertisescan.model

data class DeviceState(
    var device: Device,
    var connectionState: String,
    var cancelState: Boolean
)