package com.example.bleadvertisescan.model

data class Connection(val device: Device, val state: String, val cancel: Boolean)
