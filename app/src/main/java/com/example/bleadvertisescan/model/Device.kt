package com.example.bleadvertisescan.model

data class Device(val name: String, var address: String)