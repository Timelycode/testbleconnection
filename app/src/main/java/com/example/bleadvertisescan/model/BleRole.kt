package com.example.bleadvertisescan.model

enum class BleRole {
    None,
    Central,
    Peripheral
}