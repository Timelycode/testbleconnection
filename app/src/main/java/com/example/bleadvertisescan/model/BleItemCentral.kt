package com.example.bleadvertisescan.model

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothSocket

data class BleItemCentral(
    var characteristicRead: BluetoothGattCharacteristic? = null,
    var socket: BluetoothSocket? = null,
    var gatt: BluetoothGatt? = null,
    var psm: Int? = null
)
