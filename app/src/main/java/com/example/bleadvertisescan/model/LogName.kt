package com.example.bleadvertisescan.model

enum class LogName {
    Scan,
    Advertise,
    BleConnect
}