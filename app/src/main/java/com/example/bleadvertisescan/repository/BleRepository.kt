package com.example.bleadvertisescan.repository

import android.annotation.SuppressLint
import android.bluetooth.BluetoothManager
import android.content.Context
import com.example.bleadvertisescan.advertise.AdvertiseWorker
import com.example.bleadvertisescan.advertise.observer.AdvertiseObserver
import com.example.bleadvertisescan.connect.ConnectWorkerCentral
import com.example.bleadvertisescan.connect.ConnectWorkerPeripheral
import com.example.bleadvertisescan.connect.observer.ConnectObserver
import com.example.bleadvertisescan.model.BleRole
import com.example.bleadvertisescan.model.Device
import com.example.bleadvertisescan.scan.ScanWorker
import com.example.bleadvertisescan.scan.observer.ScanObserver

@SuppressLint("StaticFieldLeak")
object BleRepository {
    var role = BleRole.None
        private set
    private var isScanning = false

    var scanObserver = ScanObserver(hashMapOf())
    var connectObserver = ConnectObserver(hashMapOf())
    var advertiseObserver = AdvertiseObserver(hashMapOf())

    private var scanWorker: ScanWorker? = null
    private var advertiseWorker: AdvertiseWorker? = null
    private var centralWorker: ConnectWorkerCentral? = null
    private var peripheralWorker: ConnectWorkerPeripheral? = null

    fun selectRole(newRole: BleRole, bManager: BluetoothManager, context: Context) {
        role = newRole
        bManager.apply {
            if (newRole == BleRole.Central) {
                advertiseWorker?.stopAdvertise()
                advertiseWorker = null
                peripheralWorker = null

                scanWorker = ScanWorker(bluetoothAdapter = adapter)
                centralWorker = ConnectWorkerCentral(bluetoothAdapter = adapter, context = context)

                scanObserver.lostDevices()
                startScan()
            } else {
                scanWorker?.stopScan()
                centralWorker?.disconnectAll()
                scanWorker = null
                centralWorker = null

                advertiseWorker = AdvertiseWorker(advertiser = adapter.bluetoothLeAdvertiser)
                peripheralWorker = ConnectWorkerPeripheral(adapter = adapter)
                peripheralWorker?.mBluetoothGattServer = openGattServer(context, peripheralWorker)
                peripheralWorker!!.initService()

                scanObserver.lostDevices()
                startAdvertise()
            }
        }
    }

    fun startScan() {
        if (!isScanning) {
            isScanning = true
            scanWorker?.startScan()
        }
    }

    fun stopScan() {
        if (isScanning) {
            scanWorker?.stopScan()
        }
    }

    fun startAdvertise() {
        advertiseWorker?.startAdvertise()
    }

    fun stopAdvertise() {
        advertiseWorker?.stopAdvertise()
    }

    fun connectToDevice(device: Device) {
        centralWorker?.connectByMacAddress(model = device)
    }

    fun disconnectFromDevice(name: String) {
        centralWorker?.disconnect(name = name)
    }
}