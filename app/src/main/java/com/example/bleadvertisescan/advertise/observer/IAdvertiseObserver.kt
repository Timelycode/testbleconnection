package com.example.bleadvertisescan.advertise.observer

interface IAdvertiseObserver {
    fun changeStateOfAdvertising(advertising: Boolean)
}