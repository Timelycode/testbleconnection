package com.example.bleadvertisescan.advertise.observer

import com.example.bleadvertisescan.scan.observer.W2Observer

class AdvertiseObserver(
    map: HashMap<String, IAdvertiseObserver>
) : W2Observer<IAdvertiseObserver>(map = map), IAdvertiseObserver {
    override fun changeStateOfAdvertising(advertising: Boolean) {
        secureMap.forEach { sub -> sub.changeStateOfAdvertising(advertising = advertising) }
    }
}