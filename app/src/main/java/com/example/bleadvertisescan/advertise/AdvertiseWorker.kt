package com.example.bleadvertisescan.advertise

import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.*
import android.os.ParcelUuid
import android.util.Log
import com.example.bleadvertisescan.model.LogName
import com.example.bleadvertisescan.repository.BleRepository
import java.util.*


class AdvertiseWorker(private var advertiser: BluetoothLeAdvertiser) : AdvertiseCallback() {

    companion object {
        val manufacturerId = 1962
        val APP_UUID = "32D85410-8DC9-4A94-836D-022000000000"
        val psmCharacteristic = "EDA8D710-2EB6-4CFE-83A0-348FE32CCE65"
        val descriptorUUID = "00002902-0000-1000-8000-00805F9B34FB"
    }


    fun startAdvertise() {
        advertiser = BluetoothAdapter.getDefaultAdapter().bluetoothLeAdvertiser
        val settings = AdvertiseSettings.Builder()
            .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY)
            .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH)
            .setTimeout(0)
            .build()
        advertiser.startAdvertising(
            settings,
            getAdvertiseData(),
            getScanResult(byteArrayOf(22, 39, 4, 99)),
            this
        )
        BleRepository.advertiseObserver.changeStateOfAdvertising(advertising = true)
    }

    private fun getAdvertiseData(): AdvertiseData? {
        val parcelUuid = ParcelUuid(UUID.fromString(APP_UUID))
        val builder = AdvertiseData.Builder()
            .setIncludeDeviceName(false)
            .addServiceUuid(parcelUuid)
            .setIncludeTxPowerLevel(false)
        return builder.build()
    }

    private fun getScanResult(data: ByteArray): AdvertiseData? {
        return AdvertiseData.Builder()
            .setIncludeTxPowerLevel(false)
            .setIncludeDeviceName(true)
            .addManufacturerData(manufacturerId, data)
            .build()
    }

    fun stopAdvertise() {
        advertiser.stopAdvertising(this)
        BleRepository.advertiseObserver.changeStateOfAdvertising(advertising = false)
    }

    override fun onStartSuccess(settingsInEffect: AdvertiseSettings?) {
        super.onStartSuccess(settingsInEffect)
        Log.d(LogName.Advertise.name, "onStartSuccess")
    }

    override fun onStartFailure(errorCode: Int) {
        super.onStartFailure(errorCode)
        Log.d(LogName.Advertise.name, "onStartFailure $errorCode")
    }

    /*override fun onAdvertisingSetStarted(
        advertisingSet: AdvertisingSet?,
        txPower: Int,
        status: Int
    ) {
        super.onAdvertisingSetStarted(advertisingSet, txPower, status)
        Log.d(LogName.Advertise.name, "onAdvertisingSetStarted")
        BleRepository.advertiseObserver.changeStateOfAdvertising(advertising = true)
    }

    override fun onAdvertisingSetStopped(advertisingSet: AdvertisingSet?) {
        super.onAdvertisingSetStopped(advertisingSet)
        Log.d(LogName.Advertise.name, "onAdvertisingSetStopped")
        BleRepository.advertiseObserver.changeStateOfAdvertising(advertising = false)
    }

    override fun onAdvertisingDataSet(advertisingSet: AdvertisingSet?, status: Int) {
        super.onAdvertisingDataSet(advertisingSet, status)
        Log.d(LogName.Advertise.name, "onAdvertisingDataSet")
    }*/
}